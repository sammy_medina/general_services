package com.oriente.yacht;

import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.ioc.Ioc;
import org.nutz.mvc.Mvcs;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;

public class MainSetup implements Setup {

	@Override
	public void init(NutConfig nc) {
		Ioc ioc = nc.getIoc();
		Mvcs.DISABLE_X_POWERED_BY = true; // Don't show Nutz as X-Powered-By value.
		Dao dao = ioc.get(Dao.class);
		Daos.createTablesInPackage(dao, "com.oriente.dao.models", false);
	}

	@Override
	public void destroy(NutConfig nc) {

	}

}
