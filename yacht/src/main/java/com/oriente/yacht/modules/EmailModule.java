package com.oriente.yacht.modules;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.json.Json;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import com.oriente.core.constants.StatusCodes;
import com.oriente.dao.EmailLogDao;
import com.oriente.dao.models.EmailLog;

/**
 * Module for all methods related to email.
 * 
 * @author Rod Samuel Medina <samuel@oriente.com.ph>
 *
 */
@IocBean
public class EmailModule {

	@Inject
	private EmailLogDao emailLogDao;

	/**
	 * Sends an email through AWS SES, by retrieving an html template from a folder
	 * of templates. Inline images are automatically included.
	 * 
	 * For bulk sending, pass all recipients in a comma-separated String.
	 * 
	 * @param sender
	 * @param recipient
	 * @param subject
	 * @param template
	 */
	public String send(String sender, String recipient, String subject, String template, String parameters) {
		// Create Log
		EmailLog log = new EmailLog();
		log.setTemplate(template);
		log.setSender(sender);
		log.setRecipient(recipient);
		log.setSubject(subject);
		log.setParameters(parameters);

		try {
			String defaultCharSet = MimeUtility.getDefaultJavaCharset();

			Session session = Session.getDefaultInstance(new Properties());

			String html = "";

			if (!template.isEmpty()) {
				// Retrieve the email template (including images)
				Map<String, String> parameterMap = Json.fromJsonAsMap(String.class, parameters);
				String path = "email_templates/" + template;
				Path htmlPath = Paths.get(path + "/" + template + ".html");
				html = new String(Files.readAllBytes(htmlPath));

				// Add the parameters to the message template
				// String[] parameterArray = parameters.split(",");
				// for (int i = 0; i < parameterArray.length; i++) {
				// html = html.replace("[" + i + "]", parameterArray[i]);
				// }

				Set<String> keySet = parameterMap.keySet();
				for (String key : keySet) {
					String parameter = parameterMap.get(key);
					html = html.replace("{" + key + "}", parameter);
				}

				// List<String> filenames = new ArrayList<String>();
				// Stream<Path> paths = Files.walk(Paths.get(path + "/images"));
				// paths.filter(Files::isRegularFile).map(p ->
				// p.toString()).forEach(filenames::add);
				// paths.close();
			} else {
				html = parameters;
			}

			// Create a new MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Add subject, from and to lines.
			message.setSubject(subject, "UTF-8");
			message.setFrom(new InternetAddress(sender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));

			// Create a multipart/alternative child container.
			MimeMultipart msgBody = new MimeMultipart("alternative");

			// Create a wrapper for the HTML part.
			MimeBodyPart wrap = new MimeBodyPart();

			// Define the HTML part.
			MimeBodyPart htmlPart = new MimeBodyPart();
			// Encode the HTML content and set the character encoding.
			htmlPart.setContent(MimeUtility.encodeText(html, defaultCharSet, "B"), "text/html; charset=UTF-8");
			htmlPart.setDisposition("inline");
			htmlPart.setHeader("Content-Transfer-Encoding", "base64");

			// Add the HTML part to the child container.
			msgBody.addBodyPart(htmlPart);

			// Add the child container to the wrapper object.
			wrap.setContent(msgBody);

			// Create a multipart/mixed parent container.
			MimeMultipart msg = new MimeMultipart("mixed");

			// Add the parent container to the message.
			message.setContent(msg);

			// Add the multipart/alternative part to the message.
			msg.addBodyPart(wrap);

			// Define the inline images
			// for (String filename : filenames) {
			// MimeBodyPart att = new MimeBodyPart();
			// DataSource fds = new FileDataSource(filename);
			// att.setDataHandler(new DataHandler(fds));
			// att.setHeader("Content-ID", "<" + fds.getName() + ">");
			// // att.setFileName(fds.getName());
			//
			// // Add the image to the message.
			// msgBody.addBodyPart(att);
			// }

			// Instantiate an Amazon SES client, which will make the service
			// call with the supplied AWS credentials.
			AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
					.withRegion(Regions.US_WEST_2).build();
			// Print the raw email content on the console
			PrintStream out = System.out;
			message.writeTo(out);

			// Send the email.
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			message.writeTo(outputStream);
			RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

			SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);

			SendRawEmailResult response = client.sendRawEmail(rawEmailRequest);

			// Finalize Log
			log.setStatus(StatusCodes.OK);
			emailLogDao.save(log);

			return String.valueOf(response.getSdkHttpMetadata().getHttpStatusCode());
			// Display an error if something goes wrong.
		} catch (Exception ex) {
			// Finalize Log (with error)
			log.setStatus(StatusCodes.FAIL);
			emailLogDao.save(log);
			return ex.getMessage();
		}
	}
}
