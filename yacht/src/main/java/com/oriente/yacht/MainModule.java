package com.oriente.yacht;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.ChainBy;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.POST;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.annotation.SetupBy;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

import com.oriente.core.EndpointResult;
import com.oriente.yacht.modules.EmailModule;

@SetupBy(value=MainSetup.class)
@IocBy(type=ComboIocProvider.class, args={"*js", "ioc/",
                            "*anno", 
                            "com.oriente.yacht", 
                            "com.oriente.core", // So we can find the core objects
                            "com.oriente.dao",
                            "*tx"})
@Modules(scanPackage=true)
@ChainBy(args= {"chain.js"})
@At("")
@Ok("json:{quoteName:true, autoUnicode:true}")
@Fail("http:500")
@IocBean
public class MainModule {

	@Inject
	private EmailModule email;
	
	@POST
	@At
	@AdaptBy(type=JsonAdaptor.class)
	public Object sendEmail(
			@Param("sender") String sender, 
			@Param("recipient") String recipient, 
			@Param("subject") String subject, 
			@Param("template") String template, 
			@Param("parameters") String parameters) {
		EndpointResult result = new EndpointResult();
		result.setOk();
		if (!result.hasErrors()) {
			result.addData(email.send(sender, recipient, subject, template, parameters));
		}
		return result.getResult();
	}
}
