#!/bin/bash
rm -rf ~/.m2/repository/com/oriente
for d in */; do
 echo $d
 cd $d
 git pull
 mvn tomcat7:undeploy
 mvn tomcat7:deploy
 cd ..
done
