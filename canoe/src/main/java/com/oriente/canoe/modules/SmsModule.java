package com.oriente.canoe.modules;

import org.nutz.http.Request;
import org.nutz.http.Request.METHOD;
import org.nutz.http.Response;
import org.nutz.http.Sender;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.json.Json;
import org.nutz.lang.util.NutMap;

import com.oriente.core.utils.HttpUtil;
import com.oriente.dao.SmsLogDao;
import com.oriente.dao.models.SmsLog;

/**
 * Module for all methods related to sms
 * @author Rod Samuel Medina <samuel@oriente.com.ph
 *
 */
@IocBean
public class SmsModule {
	
	@Inject
	private SmsLogDao smsLogDao;
	
	private static final String SEMAPHORE_API_KEY = "823c5818c332c30f8d590c17f14ed4c7";
	private static final String SEMAPHORE_ENDPOINT = "http://api.semaphore.co/api/v4/messages";
	
	/**
	 * Sends an SMS. To enable bulk sending, concatenate all target numbers in a comma-separated String.
	 * e.g. ("09772108550,+639988407483")
	 * @param message
	 * @param msisdn
	 * @param recipient
	 * @param source
	 */
	public Object send(String source, String msisdn, String message) {
		// Generate request body
		NutMap params = new NutMap();
		params.addv("message", message);
		params.addv("number", msisdn);
		params.addv("apikey", SEMAPHORE_API_KEY);
		params.addv("sendername", source);
		String parameters = Json.toJson(params);
		
		// Create log
		SmsLog log = new SmsLog();
		log.setMessage(message);
		log.setMsisdn(msisdn);
		log.setSource(source);
		smsLogDao.save(log);
		
		Request request = Request.create(SEMAPHORE_ENDPOINT, METHOD.POST);
		request.setParams(params);
		Response response = Sender.create(request).setTimeout(10000).send();
		return response.getStatus();
		
		// Send request for SMS sending
//		return HttpUtil.sendPostRequest(SEMAPHORE_ENDPOINT, parameters);
	}
}
