package com.oriente.canoe;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.ChainBy;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.POST;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.annotation.SetupBy;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

import com.oriente.canoe.modules.SmsModule;
import com.oriente.core.EndpointResult;

@SetupBy(value=MainSetup.class)
@IocBy(type=ComboIocProvider.class, args={"*js", "ioc/",
                            "*anno", 
                            "com.oriente.canoe", 
                            "com.oriente.core",
                            "com.oriente.dao",// So we can find the core objects
                            "*tx"})
@Modules(scanPackage=true)
@ChainBy(args= {"chain.js"})
@At("")
@Ok("json:{quoteName:true, autoUnicode:true}")
@Fail("http:500")
@IocBean
public class MainModule {
	
	@Inject
	private SmsModule sms;
	
	@POST
	@At
	@AdaptBy(type=JsonAdaptor.class)
	public Object sendSms(
			@Param("source") String source, 
			@Param("msisdn") String msisdn, 
			@Param("message") String message) {
		EndpointResult result = new EndpointResult();
		result.setOk();
		result.addData(sms.send(source, msisdn, message));
		return result.getResult();
	}
	
}
