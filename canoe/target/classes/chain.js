{
    "default" : {
        "ps" : [
              "org.nutz.mvc.impl.processor.UpdateRequestAttributesProcessor",
              "org.nutz.mvc.impl.processor.EncodingProcessor",
              "org.nutz.mvc.impl.processor.ModuleProcessor",
              "org.nutz.mvc.impl.processor.ActionFiltersProcessor",
              "org.nutz.mvc.impl.processor.AdaptorProcessor",
              "org.nutz.mvc.impl.processor.MethodInvokeProcessor",
//              "ioc:corsProcessor", // Insert CorsProcessor to enable CORS access
//              "ioc:jwtProcessor", // Insert JwtProcessor to enable JWT security validation
              "org.nutz.mvc.impl.processor.ViewProcessor"
          ],
        "error" : "ioc:exceptionProcessor"
    }
}