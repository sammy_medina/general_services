var ioc = {
	conf : {
		type : "org.nutz.ioc.impl.PropertiesProxy",
		fields : {
			paths : [ "custom/" ]
		}
	},
	$aop : {
        type : 'org.nutz.ioc.aop.config.impl.JsonAopConfigration',
        fields : {
            itemList : [
                ['com.oriente.canoe.dao','.+','ioc:daoInterceptor','false']    
            ]
        }
    }
};